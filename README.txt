Entity Reference Number Widget

Provides a widget for D8 core entity reference fields where the target
is specified directly via the numeric entity ID.

